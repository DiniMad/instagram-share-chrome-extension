const onPostShareButtonClickedIgTv = () => {
    const videoElement = document.getElementsByTagName("video");

    contentSource = videoElement[0].getAttribute("src");

    copyToClipboard(contentSource);
};

const createPostShareButtonIgTv = () => {
    previousCopyButton = document.getElementById("button-copy-url");
    if (previousCopyButton)
        previousCopyButton.parentNode.removeChild(previousCopyButton);

    const postHeader = document.getElementsByTagName("header")[0];

    var postShareButton = document.createElement("Button");
    postShareButton.className = "button-copy-url";
    postShareButton.onclick = onPostShareButtonClickedIgTv;

    const buttonImage = document.createElement("img");
    buttonImage.className = "button-copy-url-image";
    buttonImage.setAttribute("src", chrome.extension.getURL("CopyIcon.svg"));

    postShareButton.appendChild(buttonImage);
    postHeader.appendChild(postShareButton);
};

const initialPostPageIgTv = () => {
    createPostShareButtonIgTv();
};

const postPageUrlIgTv = "https://www.instagram.com/tv/";

chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.page.startsWith(postPageUrlIgTv)) initialPostPageIgTv();
});

if (window.location.href.startsWith(postPageUrlIgTv)) initialPostPageIgTv();
