const copyToClipboard = text => {
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
};

const copySourceToClipboard = (element, imageIndex) => {
    const videoElment = element.getElementsByTagName("video");
    const imageElment = element.getElementsByTagName("img");

    const isVideoPost = videoElment.length > 0;

    contentSource = isVideoPost ? videoElment[0].getAttribute("src") : imageElment[imageIndex].getAttribute("src");

    copyToClipboard(contentSource);
};

const copyUrlButtonClicked = post => {
    const contents = post.getElementsByClassName("Ckrof");

    // If it's asingle content post
    if (contents.length === 0) {
        copySourceToClipboard(post, 2);
    } else {
        const nextButton = post.getElementsByClassName("_6CZji");
        const imageIndex = nextButton.length === 0 ? contents.length - 1 : contents.length - 2;
        contentElement = contents[imageIndex];
        copySourceToClipboard(contentElement, 0);
    }
};

const generateCopyButtons = () => {
    const posts = document.getElementsByTagName("article");

    for (let i = 0; i < posts.length; i++) {
        const post = posts[i];

        const postHeader = post.getElementsByTagName("header")[0];

        if (postHeader.getElementsByClassName("button-copy-url").length > 0) continue;

        var copyUrlButton = document.createElement("Button");
        copyUrlButton.className = "button-copy-url";
        copyUrlButton.onclick = () => copyUrlButtonClicked(post);

        const buttonImage = document.createElement("img");
        buttonImage.className = "button-copy-url-image";
        buttonImage.setAttribute("src", chrome.extension.getURL("CopyIcon.svg"));

        copyUrlButton.appendChild(buttonImage);
        postHeader.appendChild(copyUrlButton);
    }
};

const removeStoryPageButton = () => {
    const storyPageButton = document.getElementById("button-copy-url");
    if (storyPageButton) storyPageButton.parentNode.removeChild(storyPageButton);
};

const initiateFeedPage = () => {
    removeStoryPageButton();

    var tillContentLoaded = setInterval(function() {
        var contentElement = document.getElementsByClassName("cGcGK");
        if (!contentElement) return;
        clearInterval(tillContentLoaded);
        generateCopyButtons();
        contentElement[0].addEventListener("DOMNodeInserted", generateCopyButtons);
    }, 100);
};

const feedPageUrl = "https://www.instagram.com/";

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.page === feedPageUrl) initiateFeedPage();
});

if (window.location.href === feedPageUrl) initiateFeedPage();
