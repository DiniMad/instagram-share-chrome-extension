const onPostShareButtonClicked = () => {
    const videoElement = document.getElementsByTagName("video");
    const imageElments = document.querySelectorAll("._97aPb.wKWK0 img");
    const nextImageButton = document.getElementsByClassName("_6CZji");

    let imageIndex;
    switch (imageElments.length) {
        case 1: // If there is one image in content container
            imageIndex = 0;
            break;

        case 2: // If there is two decide base on next button.
            imageIndex = nextImageButton.length === 1 ? 0 : 1;
            break;

        case 3: // If there is three images in content container
            imageIndex = 1;
            break;
    }

    const isVideoPost = videoElement.length === 1;

    contentSource = isVideoPost ? videoElement[0].getAttribute("src") : imageElments[imageIndex].getAttribute("src");

    copyToClipboard(contentSource);
};

const createPostShareButton = () => {
    previousCopyButton = document.getElementById("button-copy-url");
    if (previousCopyButton) previousCopyButton.parentNode.removeChild(previousCopyButton);

    const postHeader = document.getElementsByTagName("header")[0];

    var postShareButton = document.createElement("Button");
    postShareButton.className = "button-copy-url";
    postShareButton.onclick = onPostShareButtonClicked;

    const buttonImage = document.createElement("img");
    buttonImage.className = "button-copy-url-image";
    buttonImage.setAttribute("src", chrome.extension.getURL("CopyIcon.svg"));

    postShareButton.appendChild(buttonImage);
    postHeader.appendChild(postShareButton);
};

const initialPostPage = () => {
    createPostShareButton();
};

const postPageUrl = "https://www.instagram.com/p/";

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.page.startsWith(postPageUrl)) initialPostPage();
});

if (window.location.href.startsWith(postPageUrl)) initialPostPage();
