const onCopyButtonClicked = () => {
    const videoElement = document.getElementsByTagName("video");
    const imageElment = document.getElementsByTagName("img");

    const isVideoPost = videoElement.length === 1;

    contentSource = isVideoPost
        ? videoElement[0].getElementsByTagName("source")[0].getAttribute("src")
        : imageElment[1].getAttribute("src");

    copyToClipboard(contentSource);
};

const createShareButton = () => {
    const body = document.getElementsByTagName("body")[0];

    previousCopyButton = document.getElementById("button-copy-url");
    if (previousCopyButton) previousCopyButton.parentNode.removeChild(previousCopyButton);

    var copyUrlButton = document.createElement("Button");
    copyUrlButton.id = "button-copy-url";
    copyUrlButton.onclick = onCopyButtonClicked;

    const buttonImage = document.createElement("img");
    buttonImage.className = "button-copy-url-image";
    buttonImage.setAttribute("src", chrome.extension.getURL("CopyIcon.svg"));

    copyUrlButton.appendChild(buttonImage);
    body.appendChild(copyUrlButton);
};

const initiateStoryPage = () => {
    createShareButton();
};

const storyPageUrl = "https://www.instagram.com/stories/";

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    if (request.page.startsWith(storyPageUrl)) initiateStoryPage();
});

if (window.location.href.startsWith(storyPageUrl)) initiateStoryPage();
